﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InfoBox : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    [Header("Text box")]
    [SerializeField] private GameObject textBox_Template;
    [SerializeField] private string textBox_Title;
    [SerializeField] private string textBox_Content;
    
    [Header("Editor only")]
    [SerializeField] private bool drawGizmos = true;

    GameObject textBox;
    GameObject i_textBox;

    private void Start() {
        Image image = gameObject.AddComponent<Image>();
        image.color = new Color(0, 0, 0, 0); //Trasparent

        textBox = textBox_Template;
        TextBox textBoxEdit = textBox.GetComponent<TextBox>();
        textBoxEdit.SetTitle(textBox_Title);
        textBoxEdit.SetTitle(textBox_Content);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        Debug.Log("Enter");
        //GameObject TextBoxHolder = Instantiate(new GameObject, this.transform.)
        i_textBox = Instantiate(textBox, this.transform.position, Quaternion.identity, this.transform);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Exit");
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    // Debugging
    private void OnDrawGizmos() {
        if (drawGizmos) {
            RectTransform rt = GetComponent<RectTransform>();
            Vector3 BoxSize = new Vector3(rt.sizeDelta.x * rt.localScale.x, rt.sizeDelta.y * rt.localScale.y, 1);

            Matrix4x4 cubeTransform = Matrix4x4.TRS(transform.position, transform.rotation, BoxSize);
            Gizmos.matrix *= cubeTransform;
            Gizmos.color = new Color(255, 0, 0, 1); // Red
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        }
    }
}
