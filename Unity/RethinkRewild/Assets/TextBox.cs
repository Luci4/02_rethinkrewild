﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextBox : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI content;

    public void SetTitle(string s) {
        if (s == "") { return; }
        title.SetText(s);
    }

    public void SetContent(string s) {
        if(s == "") { return;  }
        content.SetText(s);
    }
}
