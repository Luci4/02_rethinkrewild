﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    [Header("Buttons")]
    [SerializeField] private Button button_0 = null;
    [SerializeField] private Button button_1 = null;
    [SerializeField] private Button button_2 = null;
    [SerializeField] private GameObject rewildButton = null;
    [Header("CanvasGroups")]
    [SerializeField] private CanvasGroup mainMenu = null;
    [SerializeField] private CanvasGroup bizon = null;
    [SerializeField] private CanvasGroup beaver = null;
    [SerializeField] private CanvasGroup lynx = null;
    [Header("Backgrounds")]
    [SerializeField] private Animator backgroundAnimator = null;
    [Header("Settings")]
    [SerializeField] private float fadeDuration = 1.0f;

    private enum Environment { bizon, beaver, lynx }

    private bool isInMainMenu = true;
    private bool isInEnviroment = false;
    private Environment selectedEnvironment;

    private bool hasPlayedBackgroundDrawingAnimaton = false;
    private bool hasDisplayedRewildButton = false;
    private bool hasPlayedRewildAnimaton = false;
    private bool hasCalledSetupEnvironmentInteractables = false;

    private static string IS_BIZON = "isBizon";
    private static string IS_BEAVER = "isBeaver";
    private static string IS_LYNX = "isLynx";

    private CanvasGroup rewildButton_CanvasGroup;
    private Button rewildButton_Button;

    private void Start() {
        button_0.onClick.AddListener(OnClickButton0);
        button_1.onClick.AddListener(OnClickButton1);
        button_2.onClick.AddListener(OnClickButton2);

        rewildButton_CanvasGroup = rewildButton.GetComponent<CanvasGroup>();
        rewildButton_CanvasGroup.alpha = 0f;
        rewildButton_Button = rewildButton.GetComponent<Button>();
        rewildButton_Button.onClick.AddListener(OnClickRewildButton);
        rewildButton.SetActive(false);
    }

    private void Update() {
        if(!isInMainMenu && !isInEnviroment) {
            isInEnviroment = true;
            LoadEnviroment();
        }        

        // TODO Replace this with a button
        if (Input.GetKeyDown(KeyCode.Escape)) {
            BackToMainMenu();
        }
    }
     
    // TODO Add a back button
    public void BackToMainMenu() {
        bizon.gameObject.SetActive(false);
        beaver.gameObject.SetActive(false);
        lynx.gameObject.SetActive(false);
        mainMenu.alpha = 1f;

        hasPlayedBackgroundDrawingAnimaton = false;
        hasDisplayedRewildButton = false;
        hasPlayedRewildAnimaton = false;
        hasCalledSetupEnvironmentInteractables = false;

        isInMainMenu = true;
        isInEnviroment = false;

        backgroundAnimator.SetBool(IS_BIZON, false);
        backgroundAnimator.SetBool(IS_BEAVER, false);
        backgroundAnimator.SetBool(IS_LYNX, false);

        rewildButton_CanvasGroup.alpha = 0f;
        rewildButton.SetActive(false);
    }

    private void OnClickButton0() {
        StartCoroutine(FadeAlpha(mainMenu, 1f, 0f, fadeDuration));
        bizon.gameObject.SetActive(true);
        selectedEnvironment = Environment.bizon;
    }

    private void OnClickButton1() {
        StartCoroutine(FadeAlpha(mainMenu, 1f, 0f, fadeDuration));
        beaver.gameObject.SetActive(true);
        selectedEnvironment = Environment.beaver;
    }

    private void OnClickButton2() {
        StartCoroutine(FadeAlpha(mainMenu, 1f, 0f, fadeDuration));
        lynx.gameObject.SetActive(true);
        selectedEnvironment = Environment.lynx;
    }

    private void OnClickRewildButton() {
        // - play rewild animation
        //   - call setup environment interactables (from another class)
    }

    private void LoadEnviroment() {
        if(selectedEnvironment == Environment.bizon) {
            StartEnviroment(Environment.bizon);
        } else if (selectedEnvironment == Environment.beaver) {
            StartEnviroment(Environment.beaver);
        } else if (selectedEnvironment == Environment.lynx) {
            StartEnviroment(Environment.lynx);
        }
    }

    private void StartEnviroment(Environment e) {
        print("StartEnviroment");

        if (e == Environment.bizon) {
            backgroundAnimator.SetBool(IS_BIZON, true);
        } else if (e == Environment.beaver) {
            backgroundAnimator.SetBool(IS_BEAVER, true);
        } else if (e == Environment.lynx) {
            backgroundAnimator.SetBool(IS_LYNX, true);
        }
        rewildButton.SetActive(true);
        StartCoroutine(FadeAlpha(rewildButton_CanvasGroup, 0f, 1f, 1f));
    }

    IEnumerator FadeAlpha(CanvasGroup cg, float start, float end, float duration) {
        for (float t = 0f; t < duration; t += Time.deltaTime) {
            float normalizedTime = t / duration;
            cg.alpha = Mathf.Lerp(start, end, normalizedTime);
            yield return null;
        }
        cg.alpha = end;
        isInMainMenu = false;
    }


}
