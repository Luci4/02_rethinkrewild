﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalScrollBox : MonoBehaviour {

    [Header("Non-optional")]
    [SerializeField] private Button button0 = null;
    [SerializeField] private Button button1 = null;
    [SerializeField] private Button button2 = null;

    [Header("Optional")]
    [SerializeField] private Button prevButton = null;
    [SerializeField] private Button nextButton = null;

    [Header("Settings")]
    [SerializeField] private float transitionSpeedInSeconds = 0.2f;

    [SerializeField] private float ceterButtonScale = 1;
    [SerializeField] private float oneFromCeterButtonScale = 0.8f;
    [SerializeField] private float twoFromCenterButtonScale = 0.5f;

    [SerializeField] private float ceterButtonPosition = 0;
    [SerializeField] private float oneFromCeterPosition = 500;
    [SerializeField] private float twoFromCenterPosition = 700;

    // Consitter to make isMoveing and isScaling the same variable
    private bool isMoveing = false; 
    private bool isScaling = false;

    private int currentButtonIndex = 1;

    private void Start() {
        if (nextButton) {
            nextButton.GetComponent<Button>().onClick.AddListener(() => { NextButton(); });
        }

        if (prevButton) {
            prevButton.GetComponent<Button>().onClick.AddListener(() => { PreviousButton(); });
        }
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.LeftArrow)) {
            PreviousButton();
        } else if (Input.GetKeyUp(KeyCode.RightArrow)) {
            NextButton();
        }
    }

    private IEnumerator MoveToPosition(Transform transform, Vector3 target, float timeToMove) {
        isMoveing = true;
        var currentPos = transform.localPosition;
        var t = 0f;
        while (t < 1) {
            t += Time.deltaTime / timeToMove;
            transform.localPosition = Vector3.Lerp(currentPos, target, t);
            if (t >= 1) { isMoveing = false; }
            yield return null;
        }
    }

    private IEnumerator ScaleOverTime(Transform transform, float targetScale, float timeToScale) {
        isScaling = true;
        var currentScale = transform.localScale;
        var t = 0f;
        while (t < 1) {
            t += Time.deltaTime / timeToScale;
            transform.localScale = Vector3.Lerp(currentScale, new Vector3(targetScale, targetScale, targetScale), t);
            if (t >= 1) { isScaling = false; }
            yield return null;
        }
    }

    private void MoveAndScale() {
        if (currentButtonIndex == 0) {
            // 0
            StartCoroutine(MoveToPosition(button0.transform, new Vector3(ceterButtonPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button0.transform, ceterButtonScale, transitionSpeedInSeconds));
            button0.interactable = true;
            // 1
            StartCoroutine(MoveToPosition(button1.transform, new Vector3(oneFromCeterPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button1.transform, oneFromCeterButtonScale, transitionSpeedInSeconds));
            button1.interactable = false;
            // 2
            StartCoroutine(MoveToPosition(button2.transform, new Vector3(twoFromCenterPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button2.transform, twoFromCenterButtonScale, transitionSpeedInSeconds));
            button2.interactable = false;
        } else if (currentButtonIndex == 1) {
            // -1
            StartCoroutine(MoveToPosition(button0.transform, new Vector3(-oneFromCeterPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button0.transform, oneFromCeterButtonScale, transitionSpeedInSeconds));
            button0.interactable = false;
            // 0
            StartCoroutine(MoveToPosition(button1.transform, new Vector3(ceterButtonPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button1.transform, ceterButtonScale, transitionSpeedInSeconds));
            button1.interactable = true;
            // 1
            StartCoroutine(MoveToPosition(button2.transform, new Vector3(oneFromCeterPosition, 0, 0), transitionSpeedInSeconds)); 
            StartCoroutine(ScaleOverTime(button2.transform, oneFromCeterButtonScale, transitionSpeedInSeconds));
            button2.interactable = false;
        } else if (currentButtonIndex == 2) {
            // -2
            StartCoroutine(MoveToPosition(button0.transform, new Vector3(-twoFromCenterPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button0.transform, twoFromCenterButtonScale, transitionSpeedInSeconds));
            button0.interactable = false;
            // -1
            StartCoroutine(MoveToPosition(button1.transform, new Vector3(-oneFromCeterPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button1.transform, oneFromCeterButtonScale, transitionSpeedInSeconds));
            button1.interactable = false;
            // 0
            StartCoroutine(MoveToPosition(button2.transform, new Vector3(ceterButtonPosition, 0, 0), transitionSpeedInSeconds));
            StartCoroutine(ScaleOverTime(button2.transform, ceterButtonScale, transitionSpeedInSeconds));
            button2.interactable = true;

        }
    }

    private void NextButton () {
        if (!isMoveing && currentButtonIndex < 2) {
            StopAllCoroutines();
            currentButtonIndex += 1;
            MoveAndScale();
        }
    }

    private void PreviousButton() {
        if (!isMoveing && currentButtonIndex > 0) {
            StopAllCoroutines();
            currentButtonIndex -= 1;
            MoveAndScale();
        }
    }
}
