﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    [SerializeField] private Button Button1 = null;
    [SerializeField] private Button Button2 = null;
    [SerializeField] private Button Button3 = null;
    [SerializeField] private string SceneName_1 = null;
    [SerializeField] private string SceneName_2 = null;
    [SerializeField] private string SceneName_3 = null;

    private void Start() {
        Button1.onClick.AddListener(OnClickButton1);
        Button2.onClick.AddListener(OnClickButton2);
        Button3.onClick.AddListener(OnClickButton3);
    }

    private void OnClickButton1 () {
        SceneManager.LoadScene(SceneName_1);
    }

    private void OnClickButton2() {
        SceneManager.LoadScene(SceneName_2);
    }

    private void OnClickButton3() {
        SceneManager.LoadScene(SceneName_3);
    }
}
